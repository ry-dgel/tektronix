import os
import time
import numpy as np

class usbtmc:
    """Simple implementation of a USBTMC device driver, in the style of visa.h"""

    def __init__(self, device):
        self.device = device
        self.FILE = os.open(device, os.O_RDWR)

        # TODO: Test that the file opened

    def write(self, command):
        os.write(self.FILE, command.encode('utf-8'));

    def read(self, length = 4000):
        return os.read(self.FILE, length)

    def getName(self):
        self.write("*IDN?")
        return self.read(300)

    def sendReset(self):
        self.write("*RST")


class TekScope1000:
    """Class to control a Tektronix TDS1000 series oscilloscope"""
    def __init__(self, device):
        self.meas = usbtmc(device)

        self.name = self.meas.getName()

        self.sleep = 0.01

        print(self.name)

    def set_sleep(self, time=0.01):
        self.sleep = time

    def write(self, command):
        """Send an arbitrary command directly to the scope"""
        self.meas.write(command)

    def read(self, length = 4000):
        """Read an arbitrary amount of data directly from the scope"""
        try: 
            time.sleep(self.sleep)
            return self.meas.read(length)
        except TimeoutError:
            print("Error: timeout. Invalid command or sleep time too short.")

    def reset(self):
        """Reset the instrument"""
        self.meas.sendReset()

    def read_data(self):
        """ Function for reading data and parsing binary into np array """
        self.write("CURV?")
        rawdata = self.read(9000)

        # First few bytes are characters to specify
        # the length of the transmission. Need to strip these off:
        # we'll assume a 5000-byte transmission
        # so the string would be "#45000" and we therefor strip 6 bytes.
        data_array = np.frombuffer(rawdata[6:-1], 'i2')

        # If the scope is in 'scan' mode for timescales > 
        # The scan empty area is returned as -32640
        # Set this to be NaN instead for better averageing
        data_float = np.array(data_array, dtype=np.float)
        data_float[data_float<-32620] = np.nan

        return data_float


    def get_data(self,source):
        """
        Get scaled data from source where source is one of
        CH1,CH2,REFA,REFB
        """

        self.write("DATA:SOURCE " + source)
        data = self.read_data()

        # Get the voltage scale
        self.write("WFMP:" + source + ":YMULT?")
        ymult = float(self.read(20))

        # And the voltage offset
        self.write("WFMP:" + source + ":YOFF?")
        yoff = float(self.read(20))

        # And the voltage zero
        self.write("WFMP:" + source + ":YZERO?")
        yzero = float(self.read(20))
        
        # Added by Rigel: For TBS1154 scope, need to divide scale by 256
        # Due to digital mumbo jumbo
        data = ((data - yoff) * ymult / 256) + yzero
        return data

    def get_xdata(self):
        """Method to get the horizontal data array from a scope"""
        # Get the timescale
        self.write("HORIZONTAL:MAIN:SCALE?")
        timescale = float(self.read(20))

        # Get the timescale offset
        self.write("HORIZONTAL:MAIN:POSITION?")
        timeoffset = float(self.read(20))

        # Get the length of the horizontal record
        self.write("HORIZONTAL:RECORDLENGTH?")
        time_size = int(self.read(30))

        time = np.arange(0,timescale*10,timescale*10/time_size*2) # For some reason the output was too short compared to the data buffer
        return time

    #Code added by Rigel starting 18-08-08

    #Read multiple shots of data
    def multi_data(self, source, shots):
        #Stick together 'shots' number of data reads
        y_data = np.concatenate(list(map(lambda x: self.get_data(source), 
                                         range(shots))))
        return self.multi_xdata(shots), y_data

    def multi_xdata(self, shots):
        # Get single x axis data
        x_data_sing = self.get_xdata()
        # Figure out time spanned
        delta_x = x_data_sing[-1] - x_data_sing[0]
        # Get initial time
        offset = x_data_sing[0]
        # make (intial_t, final_t, total # of points)
        return np.linspace(offset, 
                           delta_x * shots + offset, 
                           len(x_data_sing) * shots)

    #Read data for ~>time seconds.
    #Will incure a delay of a single read before starting.
    #Make sure you have the timescale set properly
    def read_for_time(self, source, time=1):
        # Get single x axis data
        x_data_sing = self.get_xdata()
        # Figure out time spanned
        delta_x = x_data_sing[-1] - x_data_sing[0]
        # Get initial time
        offset = x_data_sing[0]
        # Get number of shots such that total time is closest to and
        # greater than given measurement time
        shots = int(np.ceil(time / delta_x))

        return self.multi_data(source, shots)

    def time_multi(self, source, shots):
        
        times = []
        data = []

        #Initial time
        init = time.time()

        for _ in range(shots):
            times.append(time.time() - init)
            data.append(self.get_data(source))
        
        y_data = np.concatenate(data)

        x_data_sing = self.get_xdata()
        x_data = np.concatenate([x_data_sing + time - x_data_sing[0] 
                                 for time in times])

        return x_data, y_data

    def time_time(self, source, t=1):

        times = []
        data = []

        init = time.time()

        while time.time() - init < t:
            times.append(time.time() - init)
            data.append(self.get_data(source))

        y_data = np.concatenate(data)

        x_data_sing = self.get_xdata()
        x_data = np.concatenate([x_data_sing + time - x_data_sing[0] 
                                 for time in times])

        return x_data, y_data
    
    # Measures the average value of each data set taken
    # With 'shots' data sets
    # Sets the time position of each shot to be the midpoint of the interval
    def multi_avg(self, source, shots):
        #Stick together 'shots' number of averages of data reads
        data = []
        for _ in range(shots):
            data.append(np.nanmean(self.get_data(source)))
        y_data = np.array(data)

        x_data_sing = self.get_xdata()
        delta_x = x_data_sing[-1] - x_data_sing[0]
        x_data = np.arange(shots) * delta_x + delta_x/2 + x_data_sing[0]
        
        return x_data, y_data
